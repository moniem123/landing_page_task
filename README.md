# Interview Task

### After Installing

###### run following commands
```
npm install
npm run dev 
```
### After migrate Database
###### run following commands to get setting data
```
php artisan db:seed
```
you can control on landing page from [login](http://localhost:8000/login)
and you can create account from [register](http://localhost:8000/register)
