<?php
use Illuminate\Support\Facades\Storage;
use App\Models\Setting;
/**
 * @param $folder_name
 * @return string
 */
function uploadWithPath($folder_name)
{
    return 'photos/'.$folder_name;
}

/**
 * @param $filename
 * @return string
 */
function getImg($filename)
{
    $base_url = url('/');
    return $base_url.'/storage/'.$filename;
}


/**
 * @param $file
 * @param $folder
 * @return mixed
 */
function uploaderWithPath($file, $folder)
{
    $fileHash = str_replace('.' . $file->extension(), '', $file->hashName());
    $fileName = $fileHash . '.' . $file->getClientOriginalExtension();
    $path = Storage::disk('public')->putFileAs(uploadWithPath($folder), $file, $fileName);
    return $path;
}

/**
 * @param $name
 * @return string
 */
function getSetting($name)
{
    $setting = Setting::where('name', $name)->first();
    if (!$setting) return "";
    return $setting->value;
}


/**
 * @param $banner
 * @return string
 */
function checkHasImage($banner){

    if($banner && $banner->image)
        return  getImg($banner->image);
    return 'https://i1.pngguru.com/preview/137/834/449/cartoon-cartoon-character-avatar-drawing-film-ecommerce-facial-expression-png-clipart.jpg';
}
