<?php

namespace App\Http\Traits;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

trait SettingOperation
{


    /**
     * Update Existing Setting
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function RegisterSetting(Request $request){
        $data = $request->except('_token');

        foreach ($data as $key => $value) {
            if(is_array($value)){
                Validator::make($data,[
                    $key=>'required|array',
                    $key.'.*'=>'required|string',
                ])->validate();
            }else{
                Validator::make($data,[
                    $key=>'required|string',
                ])->validate();
            }

//            dd($data,$key,$value);
            if($key == '_token' || !$value) continue;
            {
                if ($request->file($key))
                {
                    $path = \Storage::disk('public')->putFile(uploadWithPath('settings'), $value);
                    Setting::where(['name'  => $key])->update(['ar_value' =>$path,'en_value'=>$path]);
                }
                else{
                    if(is_array($value))
                        Setting::where(['name'  => $key])->update(['ar_value' => $value[0],'en_value'=>(isset($value[1]))?$value[1]:$value[0]]);
                    else
                        Setting::where(['name'  => $key])->update(['ar_value' => $value,'en_value'=>$value]);
                }
            }

        }
    }

}
