<?php

namespace App\Http\Controllers;

use App\Models\AppBanner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;

class BannersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.app-banner.index',['banners'=>AppBanner::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.app-banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(),[
            'image'=>'required|image|mimes:jpeg,png,jpg|max:2048'
        ])->validate();
        $inputs['image'] = uploaderWithPath($request['image'],'banners');
        AppBanner::create($inputs);
        return redirect()->route('app-banners.index')->with('success','uploaded successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(AppBanner $app_banner)
    {
        return view('admin.app-banner.edit',compact('app_banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  AppBanner $app_banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppBanner $app_banner)
    {
        Validator::make($request->all(),[
            'image'=>'required|image|mimes:jpeg,png,jpg|max:2048'
        ])->validate();
        $inputs['image'] = uploaderWithPath($request['image'],'banners');
        $app_banner->update($inputs);
        return redirect()->route('app-banners.index')->with('success','uploaded successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  AppBanner $app_banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppBanner $app_banner)
    {
        $app_banner->delete();
       return response()->json(['status'=>true,'message'=>'deleted successfully']);
    }
}
