<?php

namespace App\Http\Controllers;

use App\Http\Traits\SettingOperation;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    use SettingOperation;

    public function __construct()
    {
        //
    }
    /**
     * Display a listing of the resource.
     *

     */
    public function index()
    {
        $pages = Setting::all()->pluck('slug')->unique();
        return view('admin.settings.index', compact('pages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     */
    public function store(Request $request)
    {
        $this->RegisterSetting($request);
        return redirect()->route('settings.index')->with('success', 'updated successfully');
    }


    public function show($id)
    {
        $settings = Setting::whereSlug($id)->get();
        $page = $id;
//        dd($settings[0]);
        return view('admin.settings.show', compact('settings', 'page'));
    }


}
