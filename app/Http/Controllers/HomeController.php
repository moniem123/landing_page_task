<?php

namespace App\Http\Controllers;

use App\Models\AppBanner;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $banners = AppBanner::where('is_ban',0)->get();
        return view('site.home.index',compact('banners'));
    }
}
