<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            ['name'=>'face','type'=> 'url','ar_value'=> 'https://facebook.com/','en_value' => 'https://facebook.com/', 'page'=> 'contact','slug'=> 'contact','ar_title'=> 'فيس بوك','en_title'=> 'facebook'],

            ['name'=> 'inst','type'=> 'url', 'ar_value'=>'https://www.instagram.com/','en_value' => 'https://www.instagram.com/', 'page'=>'contact', 'slug'=>'contact', 'ar_title'=>'انسقرام', 'en_title'=>'instagram'],
            ['name'=> 'twitter','type'=> 'url', 'ar_value'=>'http://twitter.com/','en_value' => 'http://twitter.com/', 'page'=>'contact', 'slug'=>'contact','ar_title'=> 'توتر','en_title'=> 'twitter'],
            ['name'=> 'about','type'=> 'long_text',
                'ar_value'=> '<p>تطبيق &quot; vcv&quot; يقدم الباحثين عن العمل بأسلوب جديد عن طريق تسجيل حساب خاص بالمتقدم يتضمن فيديو تعريفي للباحث بالإضافة الي البيانات الشخصية والاجتماعية و المؤهلات والخبرات السابقة ،بما يتيح إضافة معلومات قيمة تسهل على الجهة الباحثة عن فرص توظيف تنقية الاختيارات وتوفير الوقت، وأيضاً تساعد على تحديد مدى توافق المتقدمين مع الوظيفة قبل عمل مقابلة شخصية &quot;interview&quot;، وهي طريقة لم يسبق استخدامها ولم تطرح من قبل في سوق العمل، ولذلك يعد تطبيق &quot;vcv&quot; ألية توظيف جديدة لسلسة من القطاعات الحكومية والخاصة وللباحثين عن العمل على مستوى العالم تبحث عن وظيفة؟ أنشأ حساب خاص بك وقم بإضافة فيديو تعريفي واختار الباقة المميزة</p>',
                'en_value'=>'<p>The &ldquo;vcv&rdquo; application provides job seekers in a new way by registering an account for the applicant that includes an introductory video for the researcher in addition to personal and social data, qualifications and previous experiences, allowing the addition of valuable information that facilitates the entity looking for employment opportunities, filtering choices and saving time, and also helps to Determining the applicants&rsquo; compatibility with the job before conducting an &ldquo;interview&rdquo;, which is a method that has never been used and has not been presented before in the labor market. Therefore, the application of &ldquo;vcv&rdquo; is a new recruitment mechanism for a series of government and private sectors and for job seekers around the world looking for a job ? Create your own account, add an intro video and choose the premium package</p>',
                'page'=>'about','slug'=> 'about','ar_title'=> 'من نحن','en_title'=> 'about'],
            ['name'=> 'advantages','type'=> 'long_text',
                'ar_value'=> '<ul>\r\n	<li>سهولة الاستخدام</li>\r\n	<li>الحفاظ علي خصوصة البيانات للمتقدمين</li>\r\n	<li>سرعة الحصول علي الوظيفة</li>\r\n	<li>اتاحة الوصول لعدد كبير من الشركات</li>\r\n</ul>',
                'en_value'=>'<ul>\r\n	<li>Ease of use</li>\r\n	<li>Maintaining data privacy for applicants</li>\r\n	<li>The speed of getting a job</li>\r\n	<li>Giving access to a large number of companies</li>\r\n</ul>',
                'page'=>'advantages','slug'=> 'advantages', 'ar_title'=>'مزايا','en_title'=> 'advantages'],
            ['name'=>'phone','type'=> 'number','ar_value'=> '06155','en_value' => '06155', 'page'=>'contact', 'slug'=>'contact', 'ar_title'=>'رقم الهاتف', 'en_title'=>'phone'],
            ['name'=>'email','type'=> 'email','ar_value'=> 'luciqamu@mailinator.com','en_value' => 'luciqamu@mailinator.com', 'page'=>'contact', 'slug'=>'contact', 'ar_title'=>'البريد الاليكترونى', 'en_title'=>'email'],
            ['name'=>'work_step1','type'=> 'text','ar_value'=> 'سجل فيديو تعريفي عن أهم معلوماتك ومعبر عن امكانياتك','en_value' => 'Record an introductory video about your most important information and express your capabilities', 'page'=>'work steps', 'slug'=>'work steps', 'ar_title'=>'سجل فيديو تعريفي عن أهم معلوماتك ومعبر عن امكانياتك', 'en_title'=>'record video'],
            ['name'=>'work_step2','type'=> 'text','ar_value'=> 'تقدم للوظائف المعروضة المناسبة وفقا لمهاراتك المدخلة مسبقا','en_value' => 'Apply for the appropriate offered jobs according to your previously entered skills', 'page'=>'work steps', 'slug'=>'work steps', 'ar_title'=>'تقدم للوظائف المعروضة المناسبة وفقا لمهاراتك المدخلة مسبقا', 'en_title'=>'Apply for jobs'],
            ['name'=>'work_step3','type'=> 'text','ar_value'=> 'انتظر تواصل الشركات معك عبر الدردشة الخاصة بالتطبيق','en_value' => 'Wait for companies to contact you via the application\'s chat الدردشة','page'=> 'work steps','slug'=> 'work steps','ar_title'=> 'انتظر تواصل الشركات معك عبر الدردشة الخاصة بالتطبيق','en_title'=> 'Job acceptance'],
            ['name'=>'ios_app','type'=> 'url','ar_value'=> 'https://www.apple.com/', 'en_value' =>'https://www.apple.com/','page'=> 'contact','slug'=> 'contact','ar_title'=> 'متجر ابل','en_title'=> 'app store'],
            ['name'=>'android_app','type'=> 'url','ar_value'=> 'https://play.google.com/','en_value' => 'https://play.google.com/','page'=> 'contact','slug'=> 'contact','ar_title'=> 'متجر جوجل','en_title'=> 'google play']

        ];

           Setting::insert($settings);


    }
}
