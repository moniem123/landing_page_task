<section id="section-1-2" class="work-process colored">
    <div class="container">

        <div class="section-header text-center">
            <h2>
                كيف يعمل
            </h2>
            <p>
                التطبيق سهل الإستخدام فهي خطوات بسيطة للتعامل مع التطبيق
            </p>
        </div>

        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="item text-center">
                    <div class="icon-wrap">
                        <span class="step s1-1" data-sr-id="5" style="; visibility: visible;  -webkit-transform: scale(1.5); opacity: 0;transform: scale(1.5); opacity: 0;">1</span>
                        <span class="icon-tools-2"></span>
                    </div>
                    <h3>تسجيل فيديو</h3>
                    <p>سجل فيديو تعريفي عن أهم معلوماتك ومعبر عن امكانياتك</p>

                </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="item text-center">
                    <div class="icon-wrap">
                        <span class="step s1-2" data-sr-id="6" style="; visibility: visible;  -webkit-transform: scale(1.5); opacity: 0;transform: scale(1.5); opacity: 0;">2</span>
                        <span class="icon-tools"></span>
                    </div>
                    <h3>تقدم للوظائف</h3>
                    <p>تقدم للوظائف المعروضة المناسبة وفقا لمهاراتك المدخلة مسبقا</p>

                </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="item text-center">
                    <div class="icon-wrap">
                        <span class="step s1-3" data-sr-id="7" style="; visibility: visible;  -webkit-transform: scale(1.5); opacity: 0;transform: scale(1.5); opacity: 0;">3</span>
                        <span class="icon-layers"></span>
                    </div>
                    <h3>القبول بالوظيفة</h3>
                    <p>انتظر تواصل الشركات معك عبر الدردشة الخاصة بالتطبيق</p>

                </div>
            </div>
        </div>
    </div>
</section>
