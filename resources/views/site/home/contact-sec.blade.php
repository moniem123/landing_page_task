<section id="section-1-12" class="download-cta colored">
    <div class="container container-800 z-1 relative">
        <div class="cta text-center">
            <h2 class="light">تواصل معنا</h2>
            <p>تواصل معنا بالطرق المختلفة او بالاتصال او بالبريد الإلكتروني او السوشيال ميديا</p>
            <ul class="contact-list">
                <li>
                    <a href="mailto:{{getSetting('email')}}">
                        <i class="fa fa-envelope" aria-hidden="true"></i> {{getSetting('email')}}
                    </a>
                </li>
                <li>
                    <a href="tel:{{getSetting('phone')}}">
                        <i class="fa fa-phone" aria-hidden="true"></i> {{getSetting('phone')}}
                    </a>
                </li>
            </ul>

            <!-- buttons -->
            <div class="cta-buttons">
                <a href="{{getSetting('ios_app')}}" class="btn btn-primary btn-download hvr-float-shadow">
                    <i class="fa fa-apple" aria-hidden="true"></i>
                    <span class="text">
						<span class="little">Download on the</span><br>App Store
					</span>
                </a>
                <a href="{{getSetting('android_app')}}" class="btn btn-primary btn-download hvr-float-shadow">
                    <i class="fa fa-android" aria-hidden="true"></i>
                    <span class="text">
						<span class="little">Get it on</span><br>Google Play
					</span>
                </a>
            </div>

        </div>

    </div>
</section>
