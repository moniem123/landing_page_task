<section id="section-1-3" class="describe-1">
    <div class="container">
        <div class="row flex"><!-- Row begin -->
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="image">
                    <img src="{{asset('_site/index_files/1609251781-2-01.png')}}" alt="describe" class="b20-1" data-sr-id="2" >
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <h2 class="light">مميزاتنا</h2>
                <p>يشمل التطبيق على العديد من المميزات الجيدة</p>
                <ul class="list-style pb-15">
                    {!! getSetting('advantages') !!}
                </ul>
            </div>
        </div>
    </div>
</section>
