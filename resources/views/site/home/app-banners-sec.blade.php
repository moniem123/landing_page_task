<section id="section-1-7" class="screenshots">
    <div class="container">

        <!-- section header -->
        <div class="section-header text-center">
            <h2 class="light">شاشات التطبيق</h2>
            <p>بعض شاشات التطبيق تبين سهولة التعامل و جمال التنسيق</p>
        </div>

        <!-- wrapper -->
        <div class="slider demo">
            @forelse($banners as $banner)
                <div>
                    <img src="{{getImg($banner->image)}}" alt="screenshot">
                </div>
            @empty
                <div class="container">لا تتوفر صور حاليا</div>
            @endforelse
        </div>

    </div>
</section>

@section('scripts')
    <script>
        $(document).ready(function(){
            $('.demo').slick({
                dots: true,
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 4,
                swipe: true,
                arrows: false,
            });
        });
    </script>
@endsection
