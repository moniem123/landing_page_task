<!DOCTYPE html>
<!-- saved from url=(0014)about:internet -->
<html lang="en-US"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>تطبيق فيديو سي في -  </title>
    <meta name="description" content="Pixa - App Landing Page Pack with Page builder">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('_site/index_files/1608234656-logo-new.png')}}">

    <!-- STYLESHEETS -->
    <link rel="stylesheet" href="{{asset('_site/index_files/bootstrap.min.css')}}" type="text/css" media="all">
    <link rel="stylesheet" href="{{asset('_site/index_files/et.css')}}" type="text/css" media="all">
{{--    <link rel="stylesheet" href="{{asset('_site/index_files/et-line.css')}}" type="text/css" media="all">--}}
    <link rel="stylesheet" href="{{asset('_site/index_files/magnific-popup.css')}}" type="text/css" media="all">

    <link rel="stylesheet" href="{{asset('_site/index_files/slick.css')}}" type="text/css" media="all">
    <link rel="stylesheet" href="{{asset('_site/index_files/slick-theme.css')}}" type="text/css" media="all">

    <link rel="stylesheet" href="{{asset('_site/index_files/hover-min.css')}}" type="text/css" media="all">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
{{--    <link rel="stylesheet" href="{{asset('_site/index_files/font-awesome.min.css')}}" type="text/css" media="all">--}}
    <link rel="stylesheet" href="{{asset('_site/index_files/style.css')}}" type="text/css" media="all">
    <link rel="stylesheet" href="{{asset('_site/index_files/style-rtl.css')}}" type="text/css" media="all">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('styles')
</head>
<body>
<!-- Start section header  -->
@include('site.app.header')
<!-- End section header  -->

@yield('content')

<!-- Start section footer -->
@include('site.app.footer')
<!-- End section footer -->

<!-- SCRIPTS -->
<script type="text/javascript" src="{{asset('_site/index_files/jquery-1.12.3.min.js.download')}}"></script>
<script type="text/javascript" src="{{asset('_site/index_files/jquery.easing.min.js.download')}}"></script>
<script type="text/javascript" src="{{asset('_site/index_files/jquery.single-page.min.js.download')}}"></script>
<script type="text/javascript" src="{{asset('_site/index_files/jquery.magnific-popup.min.js.download')}}"></script>
<script type="text/javascript" src="{{asset('_site/index_files/bootstrap.min.js.download')}}"></script>
<script type="text/javascript" src="{{asset('_site/index_files/jquery.counterup.min.js.download')}}"></script>
<script type="text/javascript" src="{{asset('_site/index_files/jquery.ajaxchimp.js.download')}}"></script>
<script type="text/javascript" src="{{asset('_site/index_files/countdown.js.download')}}"></script>
<script type="text/javascript" src="{{asset('_site/index_files/waypoints.min.js.download')}}"></script>

<script type="text/javascript" src="{{asset('_site/index_files/slick.min.js')}}"></script>
{{--<script type="text/javascript" src="{{asset('_site/index_files/slick.min.js.download')}}"></script>--}}

<script type="text/javascript" src="https://unpkg.com/scrollreveal@4.0.0/dist/scrollreveal.min.js"></script>
{{--<script type="text/javascript" src="{{asset('_site/index_files/scrollreveal.min.js.download')}}"></script>--}}
<script type="text/javascript" src="{{asset('_site/index_files/classie.js.download')}}"></script>
<script type="text/javascript" src="{{asset('_site/index_files/custom.js.download')}}"></script>

@yield('scripts')

</body>
</html>
