<div class="preloader-outer" style="display: none;">
    <div class="preloader" aria-busy="true" aria-label="Loading, please wait." role="progressbar"></div>
</div>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button aria-controls="navbar" aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button"><span class="sr-only">Toggle
            navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>

            <!-- Logo -->
            <a class="navbar-brand current" href="http://vcvjobs.com/#section-1-1">
                <img src="{{asset('_site/index_files/1608234656-logo-new.png')}}" alt="homepage" class="light-logo">
            </a>

            <!-- Logo end -->

        </div>
        <div class="navbar-collapse collapse" id="navbar">

            <div class="navbar-right">

                <ul class="nav navbar-nav">
                    <li>
                        <a href="http://vcvjobs.com/#section-1-1" class="current">الرئيسية</a>
                    </li>
                    <li>
                        <a href="http://vcvjobs.com/#section-1-2">كيف يعمل</a>
                    </li>
                    <li>
                        <a href="http://vcvjobs.com/#section-1-3">مميزاتنا</a>
                    </li>
                    <li>
                        <a href="http://vcvjobs.com/#section-1-7">شاشات التطبيق</a>
                    </li>
                    <li>
                        <a href="http://vcvjobs.com/#section-1-12">تواصل معنا</a>
                    </li>
                </ul>

                <!-- Menu end -->

                <!-- Social Icons -->
                <ul class="nav navbar-nav social">
                    <li><a href="{{getSetting('face')}}" target="_blank" class="external"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="{{getSetting('twitter')}}" target="_blank" class="external"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

                    <li><a href="http://vcvjobs.com/en" class="external">EN</a></li>
                </ul>

            </div>

        </div><!--/.nav-collapse -->
    </div>
</nav>
