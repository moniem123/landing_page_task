<footer id="footer-1" class="footer">
    <div class="container">

        <!-- footer menu -->
        <ul class="footer-menu">
            <li>
                <a href="http://vcvjobs.com/#section-1-1">الرئيسية</a>
            </li>
            <li>
                <a href="http://vcvjobs.com/#section-1-2">كيف يعمل</a>
            </li>
            <li>
                <a href="http://vcvjobs.com/#section-1-3">مميزاتنا</a>
            </li>
            <li>
                <a href="http://vcvjobs.com/#section-1-7">شاشات التطبيق</a>
            </li>
            <li>
                <a href="http://vcvjobs.com/#section-1-12">تواصل معنا</a>
            </li>
            <li>
                <a href="http://vcvjobs.com/en">English</a>
            </li>
        </ul>
        <!-- copyright text -->
        <span class="copyright">All Rights Received 2020 <br>
        <a href="https://moltaqa.net/">Alkayan elaseel - moltaqa tech</a></span>

        <!-- social icons -->
        <ul class="social-icons">
            <li><a href="{{getSetting('face')}}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="{{getSetting('twitter')}}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="{{getSetting('inst')}}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        </ul>

    </div>
</footer>
