@extends('admin.app.layout')

@section('content')
    <div class="min-h-screen bg-gray-100">

        <!-- Page Heading -->
        <header class="bg-white shadow">
            <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
               <span class="max-w-xl">
                   <a href="{{route('dashboard')}}">Dashboard</a>
               </span>
               <span class="float-right ml-4 ">
                   <a href="{{route('settings.index')}}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">all settings</a>
               </span>
            </div>
        </header>

        <!-- Page Content -->
        <main>
           @include('admin.alert')
           <!-- component -->
               <div class="py-12">
                   <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                       <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                           <div class="p-6 bg-white border-b border-gray-200">
                               <form method="POST" action="{{route('settings.store')}}">
                                   @csrf
                                   @foreach($settings as $setting)
                                   @if($setting->type == 'text')
                                       <div class="mb-4">
                                           <label class="text-xl text-gray-600">Ar {{$setting->title}} <span class="text-red-500">*</span></label></br>
                                           <input type="text" class="border-2 border-gray-300 p-2 w-full" name="{{$setting->name.'[]'}}" id="title" value="{{($setting->ar_value)}}" required>
                                       </div>

                                       <div class="mb-4">
                                           <label class="text-xl text-gray-600">En  {{$setting->title}} <span class="text-red-500">*</span></label></br>
                                           <input type="text" class="border-2 border-gray-300 p-2 w-full" name="{{$setting->name.'[]'}}" id="title" value="{{($setting->en_value)}}" required>
                                       </div>
                                   @endif
                                   @if($setting->type == 'number')
                                       <div class="mb-4">
                                           <label class="text-xl text-gray-600">{{$setting->title}}  <span class="text-red-500">*</span></label></br>
                                           <input type="number" class="border-2 border-gray-300 p-2 w-full" name="{{$setting->name}}" id="title" value="{{($setting->value)}}" required>
                                       </div>
                                   @endif
                                   @if($setting->type == 'email')
                                       <div class="mb-4">
                                           <label class="text-xl text-gray-600">{{$setting->title}}  <span class="text-red-500">*</span></label></br>
                                           <input type="email" class="border-2 border-gray-300 p-2 w-full" name="{{$setting->name}}" id="title" value="{{($setting->value)}}" required>
                                       </div>
                                   @endif
                                   @if($setting->type == 'url')
                                       <div class="mb-4">
                                           <label class="text-xl text-gray-600">{{$setting->title}} Link  <span class="text-red-500">*</span></label></br>
                                           <input type="text" class="border-2 border-gray-300 p-2 w-full" name="{{$setting->name}}" id="title" value="{{($setting->value)}}" required>
                                       </div>
                                   @endif
                                   @if($setting->type == 'long_text')
                                       <div class="mb-8">
                                           <label class="text-xl text-gray-600">Ar {{$setting->title}} <span class="text-red-500">*</span></label></br>
                                           <textarea name="{{$setting->name.'[]'}}" class="border-2 border-gray-500 editor">{{$setting->ar_value}}</textarea>
                                       </div>

                                       <div class="mb-8">
                                           <label class="text-xl text-gray-600">En {{$setting->title}} <span class="text-red-500">*</span></label></br>
                                           <textarea name="{{$setting->name.'[]'}}" class="border-2 border-gray-500 editor">{{$setting->en_value}}</textarea>
                                       </div>
                                   @endif
                                   @endforeach
                                   <div class="flex p-1">
                                       <button role="submit" class="p-3 bg-blue-500 text-white hover:bg-blue-400" required>Submit</button>
                                   </div>
                               </form>
                           </div>
                       </div>
                   </div>
               </div>
        </main>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

    <script>
        CKEDITOR.replaceAll( 'editor' );
    </script>
@endsection
