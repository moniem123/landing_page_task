@extends('admin.app.layout')

@section('content')
    <div class="min-h-screen bg-gray-100">

        <!-- Page Heading -->
        <header class="bg-white shadow">
            <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
               <span class="max-w-xl">
                   <a href="{{route('dashboard')}}">Dashboard</a>
               </span>
               <span class="float-right ml-4 ">
                   <a href="{{route('app-banners.index')}}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">all banner</a>
               </span>
            </div>
        </header>

        <!-- Page Content -->
        <main>
            @include('admin.alert')
            <div class="container h-screen bg-gray-200 flex justify-center items-center">
{{--                <div class="bg-white h-96 w-80 rounded-2xl">--}}
                    {!! Form::model($app_banner,['route' => ['app-banners.update',$app_banner],'method'=>'put','class'=>'relative  mt-2 text-gray-900 p-2','files'=>true]) !!}
                        @include('admin.app-banner.form')
                        <div class="text-center mt-7">
                            <button type="submit" class="bg-black text-gray-50 px-3 py-2 rounded-2xl rounded-b-none">
                               save
                            </button>
                        </div>
                    {!! Form::close() !!}
{{--                </div>--}}
            </div>
        </main>
    </div>
@endsection

@section('scripts')
    <script>
        $('document').ready(function (){
            $('.image-uploader').change(function (event) {
                $(this).parents('.upload-container').find('.img-uploaded').attr('src',URL.createObjectURL(event.target.files[0]));
                    // .append('<div class="uploaded-block"><img src="' +  + '"><button class="close">&times;</button></div>');
            });
        })
    </script>
@endsection
